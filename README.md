# Nemo Ecosystem

Place where new ideas, concepts and bugs related to the Nemo ecosystem can be shared.

Click [here](https://gitlab.com/nemo-community/nemo-ecosystem/-/issues) to see current tickets.
